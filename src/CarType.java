/**
 * Created by amen on 8/5/17.
 */
public enum CarType {
    TRUCK(1.23), CAR(1.0), MOTORCYCLE(0.72);
    private double priceMultiplier;

    CarType(double mul) {
        priceMultiplier = mul;
    }

    public double getPriceMultiplier() {
        return priceMultiplier;
    }
}
