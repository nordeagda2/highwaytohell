import java.util.Optional;
import java.util.Scanner;

/**
 * Created by amen on 8/5/17.
 */
public class Main {
    public static void main(String[] args) {
        Highway highway = new Highway();

        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String line = in.nextLine();
            String[] words = line.split(" ");

            try {
                if (words[0].toLowerCase().equals("enter")) {
                    String plates = words[1];
                    CarType carType = CarType.valueOf(words[2].toUpperCase());

                    if (highway.vehicleEntry(plates, carType)) {
                        System.out.println("Car entered into highway.");
                    } else {
                        System.out.println("Car didn't enter highway.");
                    }
                } else if (words[0].toLowerCase().equals("leave")) {
                    String plates = words[1];

                    Optional<Double> price = highway.vehicleLeave(plates);
                    if (price.isPresent()) {
                        System.out.println("Price for ride: " + price.get());
                    } else {
                        System.out.println("No price was returned");
                    }
                } else if (words[0].toLowerCase().equals("search")) {
                    String plates = words[1];

                    System.out.println(highway.searchVehicle(plates));
                }
            } catch (IllegalArgumentException iae) {
                System.out.println("Wrong car type was typed.");
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                System.out.println("Wrong number of arguments. You should provide plates and car type.");
            }
        }

    }
}
