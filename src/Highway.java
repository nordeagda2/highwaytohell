import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by amen on 8/5/17.
 */
public class Highway {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("[HH:mm yyyy/MM/dd]");
    private Map<String, VehicleInfo> carRegistry = new HashMap<>();

    public Highway() {

    }

    /**
     * Creates new record and puts it into car registry.
     *
     * @param plates - car plates.
     * @param type   - car type
     * @return - true if car can be added into registry, false if can't (car is already on highway)
     */
    public boolean vehicleEntry(String plates, CarType type) {
        if (carRegistry.containsKey(plates)) {
            System.out.println("Car already on highway");
            return false; // return fail
        }
        // create new registry entry
        VehicleInfo newCarEntry = new VehicleInfo(plates, type);
        carRegistry.put(plates, newCarEntry); // put it into registry

        return true;// return success.
    }

    public VehicleInfo searchVehicle(String plates) {
        if (!carRegistry.containsKey(plates)) {
            System.out.println("Car is not on highway");
            return null;
        }
        System.out.println("Car is found on highway.");
        System.out.println("Car time of entry: " +
                carRegistry.get(plates).getEntryTime().format(formatter));

        return carRegistry.get(plates);
    }

    /**
     * Takes plates and finds vehicle info in registry. If car exists in
     * registry it is being removed from it and total price for ride is returned.
     *
     * @param plates - car plates.
     * @return - Optional value - if present, than contains total ride price.
     */
    public Optional<Double> vehicleLeave(String plates) {
        if (!carRegistry.containsKey(plates)) {
            System.out.println("Car is not found on highway.");
            return Optional.empty();
        }

        // get info
        VehicleInfo vehicleInfo = carRegistry.get(plates); //get info
        carRegistry.remove(plates);     // remove from registry

        // count how much to pay
        long difference = Duration.between(vehicleInfo.getEntryTime(), LocalDateTime.now()).toMillis();
        double toPay = (difference / 1000.0) * vehicleInfo.getType().getPriceMultiplier();

        // return how much to pay
        return Optional.of(new Double(toPay));
    }

    public Map<String, VehicleInfo> getCarRegistry() {
        return carRegistry;
    }

    public void setCarRegistry(Map<String, VehicleInfo> carRegistry) {
        this.carRegistry = carRegistry;
    }
}
