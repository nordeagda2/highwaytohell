import java.time.LocalDateTime;

/**
 * Created by amen on 8/5/17.
 */
public class VehicleInfo {
    private String plates;
    private CarType type;
    private LocalDateTime entryTime;

    public VehicleInfo(String plates, CarType type) {
        this.plates = plates;
        this.type = type;
        // time is generated here, whenever ticket is generated
        this.entryTime = LocalDateTime.now();
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        this.plates = plates;
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public LocalDateTime getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(LocalDateTime entryTime) {
        this.entryTime = entryTime;
    }

    @Override
    public String toString() {
        return "VehicleInfo{" +
                "plates='" + plates + '\'' +
                ", type=" + type +
                ", entryTime=" + entryTime +
                '}';
    }
}
